import json
import os
import threading
import socket
import time
from boltons import socketutils
import Connection
#Nathaniel Hanson
#2/13/2018
#util.py
#Contains methods for building and sending Dronology State and Handshake Messages
#Based off code from Professor Huang and T.A. Sean Bayley

#Dronology message class
class DronologyMessage(object):
    def __init__(self, m_type, uav_id, data):
        self.m_type = m_type
        self.uav_id = uav_id
        self.data = data
    def __str__(self):
        return json.dumps({'type': self.m_type,
                           'sendtimestamp': long(round(time.time() * 1000)),
                           'uavid': str(self.uav_id),
                           'data': self.data})

    def __repr__(self):
        return str(self)

#Handshake class to instantiate a Dronology Handshake Message
class DroneHandshakeMessage(DronologyMessage):
    def __init__(self, uav_id, data, p2sac='../cfg/sac.json'):
        super(DroneHandshakeMessage, self).__init__('handshake', uav_id, data)
        self.p2sac = p2sac
    
    #Get information about current vehicle state
    @classmethod
    def from_vehicle(cls, vehicle, VID, p2sac='../cfg/sac.json'):
        battery = {
            'voltage': vehicle.battery.voltage,
            'current': vehicle.battery.current,
            'level': vehicle.battery.level,
        }

        lla = vehicle.location.global_relative_frame
        data = {
            'home': {'x': lla.lat,
                     'y': lla.lon,
                     'z': lla.alt},
            'safetycase': json.dumps({})}
        return cls(VID, data)

#Class to Extract Vehicle Information from Vehicle and put it in a library
class StateMessage(DronologyMessage):
    def __init__(self, uav_id, data):
        super(StateMessage, self).__init__('state', uav_id, data)

    @classmethod
    def from_vehicle(cls, vehicle, VID, **kwargs):
        lla = vehicle.location.global_relative_frame
        att = vehicle.attitude
        vel = vehicle.velocity
        battery = {
            'voltage': vehicle.battery.voltage,
            'current': vehicle.battery.current,
            'level': vehicle.battery.level,
        }
        data = {
            'location': {'x': lla.lat, 'y': lla.lon, 'z': lla.alt},
            'attitude': {'x': att.roll, 'y': att.pitch, 'z': att.yaw},
            'velocity': {'x': vel[0], 'y': vel[1], 'z': vel[2]},
            'status': vehicle.system_status.state,
            'heading': vehicle.heading,
            'armable': vehicle.is_armable,
            'airspeed': vehicle.airspeed,
            'groundspeed': vehicle.airspeed,
            'armed': vehicle.armed,
            'mode': vehicle.mode.name,
            'batterystatus': battery
        }

        return cls(VID, data)
